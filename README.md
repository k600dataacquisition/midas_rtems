# [MIDAS](https://bitbucket.org/tmidas/midas/src/develop/) port to [RTEMS](https://www.rtems.org/)


### Prerequisites

This port was build with rtems-4.10 ( stable ).

It requires a copy of MIDAS and rtems, as well as libbsdports for networking.

